package pl.edu.pjwstk.mpr.main;

import java.sql.SQLException;
import java.util.List;

import pl.edu.pjwstk.mpr.db.DBManager;
import pl.edu.pjwstk.mpr.domain.Student;
import pl.edu.pjwstk.mpr.ui.MenuAction;
import pl.edu.pjwstk.mpr.ui.TxtInterface;
import pl.edu.pjwstk.mpr.ui.UserInterface;

public class Start {

	private String dbName = "dziekanat";
	private UserInterface ui = new TxtInterface();
	private DBManager dbManager = new DBManager();
	
	public static void main(String[] args){
		
		new Start().go();
		
	}

	/**
	 * Przygotowania i główna pętla programu.
	 */
	private void go() {

		ui.showGreeting();
		
		try {
			dbManager.connectToDb(dbName);
		} catch (SQLException e) {
			fatalError("Nie udalo sie polaczyc do bazy " + dbName +
					"\n(" + e.getMessage() +")" +
					"\n(srawdź, czy serwer jest na pewno uruchomiony)");
			return;
		}
		
		while ( doOneAction() ) {}

	}
	
	/**
	 * Pojedyncze wyświetlenie menu i wykonanie akcji wybranej przez usera.
	 * @return true jesli user chce zakonczyc prace z programem
	 */
	private boolean doOneAction() {
		
		MenuAction action;
		do {
			action = ui.showMenuAndGetAction();
		} while (action == MenuAction.INVALID);
		
		switch (action) {
		case SHOW_ALL :
			listStudents();
			break;
		case ADD :
			Student s = ui.getStudentData();
			try {
				dbManager.saveStudentInfo(s);
				ui.showMsg("Udalo sie zapisac info o studencie "+s);
			} catch (SQLException e) {
				ui.showErrorMsg("Nie udalo sie zapisac info o studencie "+s);
				e.printStackTrace();
			}
			break;
		case DELETE :
			if (!listStudents()) break;
			int id;
			while ((id = ui.getStudentId()) < 0)
				ui.showErrorMsg("Podano niepoprawne id\n(wiadomo co ma być).");
			if (dbManager.deleteStudent(id) == true)
				break;
			else
				ui.showErrorMsg("Nie ma studenta o takim id.");
			break;
		case QUIT :
			ui.showMsg("Pa");
			return false;
		}
		
		return true;
	}
	
	/**
	 * Wyświetla listę studentów wczytaną z bazy.
	 * @return true jeśli byli jacyś studenci
	 */
	private boolean listStudents() {
		try {
			List<Student> students = dbManager.getAllStudents();
			if (students==null || students.isEmpty()) {
				ui.showMsg("W bazie nie ma żadnych studentów");
				return false;
			}
			ui.listStudents(students);
		} catch (SQLException e) {
			fatalError("Nie udało się pobrać danych studentów z bazy" +
					"\n("+e.getMessage()+")");
		}
		return true;
	}
	
	/**
	 * Reakcja na błąd uniemożliwiający dalszą pracę aplikacji
	 * @param msg wiadomość do zaprezentowania userowi
	 */
	private void fatalError(String msg) {
		ui.showErrorMsg(msg);
		ui.showErrorMsg("Ten błąd uniemożliwia dalszą pracę programu. Kończę.");
		System.exit(-1);
	}

}
