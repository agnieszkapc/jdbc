package pl.edu.pjwstk.mpr.domain;

public class Student {

	private int id = -1;
	private String name;
	private String surname;
	private String indexNo;
	private String group;

	public Student(int id, String name, String surname, String indexNo, String group) {
		this(name, surname, indexNo, group);
		this.id = id;
	}
	
	public Student(String name, String surname, String indexNo, String group) {
		this.name = name;
		this.surname = surname;
		this.indexNo = indexNo;
		this.group = group;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getIndexNo() {
		return indexNo;
	}

	public void setIndexNo(String indexNo) {
		this.indexNo = indexNo;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String toString() {
		return ((id==-1)? "": id+") ") +name+" "+surname+
			" numer indeksu:"+indexNo+" grupa:"+group;
	}

}
