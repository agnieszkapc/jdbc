package pl.edu.pjwstk.mpr.db;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.BeforeClass;
import org.junit.Test;

public class DBManagerTest {

	private static DBManager dbManager;
	
	@BeforeClass
	public static void setUp() {
		dbManager = new DBManager();
		//najbiezpieczniej byloby miec osobna baze do testow
		try {
			dbManager.connectToDb("dziekanat");
		} catch (SQLException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void connectToDbTest() {
		assertNotNull( dbManager.getCon() );
		
	}
	
	@Test
	public void tableExistsTest() {
	
		//ten test jest niepoprawny, bo zakladamy (mamy nadzieje),
		//ze bazie danych istnieje tablica 'student';
		//a pownnismy to sobie zapewnic
		
		try {
			
			assertTrue( dbManager.tableExists("student") );
			assertFalse( dbManager.tableExists("JakasNazwaTabeliOKtorejWiemyZeNieIstnieje") );
			
			assertFalse(dbManager.tableExists(null)); //zeby bylo super poprawnie powinna wyrzucic tu jakis wyjatek
			
		} catch (SQLException e) {
			
			fail("Metoda tableExists nie powinna nigdy wyrzucac wyjatkow");
			
		}
		
	}
	
}
