package pl.edu.pjwstk.mpr.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pl.edu.pjwstk.mpr.domain.Student;

public class DBManager {

	private Connection con;
	private Statement stmt;
	private PreparedStatement insertStudentStmt;
	private PreparedStatement deleteStudentStmt;
	
	public void connectToDb(String dbName) throws SQLException{
		try {
		      Class.forName("org.hsqldb.jdbc.JDBCDriver" );
		  } catch (Exception e) {
		      System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
		      e.printStackTrace();
		      return;
		  }
			con = DriverManager.getConnection(
							"jdbc:hsqldb:hsql://localhost/"+dbName, "SA", "");
			stmt = con.createStatement();
			insertStudentStmt = con.prepareStatement(
					"INSERT INTO student (name, surname, idxno, grp) VALUES (?,?,?,?)");
			deleteStudentStmt = con.prepareStatement(
					"DELETE FROM student WHERE id=?");
	}

	public Connection getCon() {
		return con;
	}

	public boolean createStudentsTable() throws SQLException {

		int updateCount = stmt.executeUpdate("CREATE TABLE student ("
				+ "id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,"
				+ "name varchar(50)," + "surname varchar(100),"
				+ "idxNo varchar(10)," + "grp varchar(10)" + ")");
		if (updateCount > 0)
			return true;
		
		return false;
		
	}
	
	boolean tableExists(String tableName) throws SQLException {
		if(tableName==null) return false;
		
		DatabaseMetaData md = con.getMetaData();
		ResultSet rs = md.getTables(null,null,tableName.toUpperCase(),null);
		if (rs.next())
			return true;
		return false;
	}

	public boolean saveStudentInfo(Student s) throws SQLException {
		if (!tableExists("student"))
			createStudentsTable();

		// tu bardziej odpowiedni bylby obiekt typu PreparedStatement...
//		int updateCount = stmt.executeUpdate(""
//				+ "INSERT INTO student (name, surname, idxno, grp) " + "VALUES ('"
//				+ s.getName() + "','" + s.getSurname() + "','" + s.getIndexNo() + "','" + s.getGroup()
//				+ "')" , Statement.RETURN_GENERATED_KEYS);
		
		insertStudentStmt.setString(1, s.getName());
		insertStudentStmt.setString(2, s.getSurname());
		insertStudentStmt.setString(3, s.getIndexNo());
		insertStudentStmt.setString(4, s.getGroup());
		
		int updateCount = insertStudentStmt.executeUpdate();
		
		ResultSet rs = stmt.getGeneratedKeys();

		if (updateCount < 1)
			return false;
		
		if(!rs.next())
			return false;
		s.setId(rs.getInt("id"));
		
		return true;
	}

	public List<Student> getAllStudents() throws SQLException {
		if(!tableExists("student")) return null;
		
		List<Student> ret = new ArrayList<Student>();
		ResultSet rs = stmt.executeQuery("SELECT * FROM student");
		while( rs.next() )
			ret.add(new Student(
					rs.getInt("id"),
					rs.getString("name"), 
					rs.getString("surname"),
					rs.getString("idxNo"),
					rs.getString("grp")));
		return ret;
	}

	/**
	 * Usuwa studenta o podanym id z bazy.
	 * @param id
	 * @return true jeśli usuwanie powiodło się (był student o takim id)
	 * @throws SQLException 
	 */
	public boolean deleteStudent(int id) throws SQLException {
		
		deleteStudentStmt.setInt(1, id);
		int uc = deleteStudentStmt.executeUpdate();
		
		if (uc>0) return true;
		
		return false;
	}
	
}
