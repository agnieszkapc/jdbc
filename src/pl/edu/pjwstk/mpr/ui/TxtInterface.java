package pl.edu.pjwstk.mpr.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import pl.edu.pjwstk.mpr.domain.Student;


public class TxtInterface implements UserInterface {

	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void showGreeting() {
		System.out.println("+------------------------------+\n"+
				           "|           Dziekanat          |\n"+
				           "+------------------------------+");
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public MenuAction showMenuAndGetAction() {
		System.out.println("\nCo checsz zrobić?\n" +
				"1. Wypisać dane studentów z bazy.\n" +
				"2. Dodać nowego studenta.\n" +
				"3. Usunąć dane studenta.\n" +
				"4. Zakończyć program\n" +
				"(wpisz cyfrę od 1 do 4)");
		String choice = getUserInput();
		return MenuAction.parse(choice);
	}

	/**
	 * {@inheritDoc}
	 */	
	@Override
	public void listStudents(List<Student> students) {
		for (Student s: students) {
			System.out.println(s);
		}
	}
	
	@Override
	public Student getStudentData() {
		System.out.println("Podaj dane studenta");
		System.out.println("imie : ");
		String name = getUserInput();
		System.out.println("nazwisko : ");
		String surname = getUserInput();
		System.out.println("numer indeksu : ");
		String indexNo = getUserInput();
		System.out.println("grupa");
		String group = getUserInput();
		return new Student(name, surname, indexNo, group);
	}
	
	@Override
	public int getStudentId() {
		System.out.println("Podaj id studenta : ");
		try {
			return Integer.parseInt(getUserInput());
		} catch (NumberFormatException e) {
			return -1;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */	
	@Override
	public void showMsg(String msg) {
		System.out.println(msg);
	}
	
	/**
	 * {@inheritDoc}
	 */	
	@Override
	public void showErrorMsg(String msg) {
		System.err.println("[BŁĄD] "+msg+"\n");
	}
	
	/**
	 * Pobiera i zwraca odpowiedź usera.
	 * @return
	 */
	private String getUserInput() {
		try {
			return br.readLine();
		} catch (IOException e) {
			System.err.println("Problem z wczytywaniem odpowiedzi użytkownika");
			System.exit(-1);
			return null;
		}
	}
	
}
