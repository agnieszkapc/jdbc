package pl.edu.pjwstk.mpr.ui;

import java.util.List;

import pl.edu.pjwstk.mpr.domain.Student;

public interface UserInterface {

	/**
	 * Wyswietla powitanie usera.
	 */
	void showGreeting();
	
	/**
	 * Wyświetla menu aplikacji i zwraca pozycję wybraną przez usera.
	 * @return
	 */
	MenuAction showMenuAndGetAction();

	/**
	 * Prezentuje userowi komunikat.
	 * @param string
	 */
	void showMsg(String msg);
	
	/**
	 * Prezentuje userowi komunikat błędu.
	 * @param string
	 */
	void showErrorMsg(String msg);

	/**
	 * Wyświetla dane studentów.
	 * @param students
	 */
	void listStudents(List<Student> students);

	/**
	 * Pobiera od użytkownika informacje o studencie.
	 * @return
	 */
	Student getStudentData();

	/**
	 * Pobiera id studenta.
	 * @return
	 */
	int getStudentId();
	
}
