package pl.edu.pjwstk.mpr.ui;

public enum MenuAction {
	SHOW_ALL,
	ADD,
	DELETE,
	QUIT,
	INVALID ;
	public static MenuAction parse(String str) {
		try {
			int ret = Integer.valueOf(str);
			switch(ret) {
				case 1: return SHOW_ALL;
				case 2: return ADD;
				case 3: return DELETE;
				case 4: return QUIT;
				default: return INVALID;
			}
		} catch (NumberFormatException e) {
			return INVALID;
		}
	}
}